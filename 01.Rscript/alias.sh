echo "alias WGCNA='Rscript /Users/shawnwang/02.MyScript/OneStepWGCNA/01.Rscript/11.09.OneStepWGCNA.R'" >> ~/.bash_alias
echo "alias GOplot='Rscript /Users/shawnwang/02.MyScript/OneStepWGCNA/01.Rscript/go.R'" >> ~/.bash_alias
echo "alias KEGGplot='Rscript /Users/shawnwang/02.MyScript/OneStepWGCNA/01.Rscript/kegg.R'" >> ~/.bash_alias
echo "alias m2gsplit='Rscript /Users/shawnwang/02.MyScript/OneStepWGCNA/01.Rscript/111.WGCNA.split.Gene.R'" >>~/.bash_alias
