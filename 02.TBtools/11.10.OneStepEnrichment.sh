#!/bin/bash
##################################################
#       Project: WGCNA enrich analysis
#       Date: Jun 28,2020
#       Author: Shawn Wang
##################################################
read -p "是否在conda环境下用Rscript(0 or 1)：" i
if [[ $i == 1 ]]; then
    read -p "R所在的conda环境名字：" env
    conda activate $env
fi
source ~/.bash_alias
## Data structure
mkdir 01.GOEnrich 02.KEGGEnrich 03.Graph 04.Data 05.tmp
## make a Gene2module working file
cp *Gene2module.xls g2m.tmp
## split geneid by module names
m2gsplit $PWD
## GO enrichment and plot for top 30 terms
cd 01.GOEnrich && cp ../05.tmp/* ./ && ls * > config
## enrichment
for i in `cat config`; do TBgo --oboFile /Users/shawnwang/02.MyScript/OneStepWGCNA/02.TBtools/go-basic.obo --gene2GoFile /Users/shawnwang/02.MyScript/OneStepWGCNA/02.TBtools/CRI.TM1.V2.TBtools.gene2go --selectionSetFiles $i; done
mkdir 01.TermCategories && mv *_Enrich* 01.TermCategories
## plot
GOplot $PWD
echo "GO 富集分析完成"
## KEGG enrichment and plot 
cd ../02.KEGGEnrich && cp ../05.tmp/* ./&& ls * >config
for i in `cat config`; do TBkegg --inKegRef /Users/shawnwang/02.MyScript/OneStepWGCNA/02.TBtools/TBtools.Plants.KEGG.Backend --Kannotation /Users/shawnwang/02.MyScript/OneStepWGCNA/02.TBtools/CRI.TM1.V2TBtools.gene2ko --selectedSet $i --outFile $i; done
    
KEGGplot $PWD
echo "KEGG富集分析完成"
cd ../
mv *.pdf 03.Graph
mv *nwk *Rdata *xls 04.Data
echo "数据整理完毕！"
