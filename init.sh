#!/bin/bash
#################################################
#		Name: Init
# 		Author: Shawn Wang
#		Date: Jun 26, 2020
#################################################
# 这个初始化脚本的目的是把其他脚本中的绝对路径改成你电脑上的绝对路径。

read -p "你的操作系统是：1. MacOS 2. linux或者win10内建的bash. 填写1或者2:" os
# 为了统一 zsh，bash or mac下的bash_profile,统一将alias写到根目录下的~/.bash_alias下
touch ~/.bash_alias
if [[ $os == 1 ]]; then
	#MacOS 操作系统
	cd 01.Rscript
	for i in `ls *`; do gsed -i "s#/Users/sirus/15.gitee/01.OneStepWGCNA/01.Rscript#$PWD#g" $i; done
    sh alias.sh
	cd ../02.TBtools
    sh alias.sh
	for i in `ls *`; do gsed -i "s#/Users/sirus/15.gitee/01.OneStepWGCNA/02.TBtools#$PWD#g" $i; done
  	echo "所有脚本中绝对路径已经更改成你的绝对路径"
elif [[ $os == 2 ]]; then
	#linux 操作系统
	cd 01.Rscript
	for i in `ls *`; do sed -i "s#/Users/sirus/15.gitee/01.OneStepWGCNA/01.Rscript#$PWD#g" $i; done
    sh alias.sh
	cd ../02.TBtools
	for i in `ls *`; do sed -i "s#/Users/sirus/15.gitee/01.OneStepWGCNA/02.TBtools#$PWD#g" $i; done
    sh alias.sh
	echo "所有脚本中绝对路径已经更改成你的绝对路径"
else
	echo "不要输入1，2以外的字符"
fi
