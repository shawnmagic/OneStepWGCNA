# OneStepWGCNA

#### 介绍
一键WGCNA，只需要准备readcount矩阵和Traitdata矩阵然后通过脚本粗略得到WGCNA中主要的结果。

#### 内容
- WGCNA基本流程
- 使用TBtools对陆地棉CRI-TM1v2为参考基因组的富集分析
- 用ggplot2对富集分析结果进行可视化，只展示`top 30`的结果

#### 准备
**WGCNA分析需要的包：**
- 个人非常不推荐用这个一键WGCNA的脚本。推荐跟着manual走一遍
- [WGCNA官网](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/)

**需要安装的R包**

```R
install.packages("getopt")
install.packages("ggplot2")
install.packages("dplyr")
install.packages("stringr")
install.packages("ape")
install.packages("reshape2")
install.packages("BiocManager")
BiocManager::install("WGCNA")
BiocManager::install("DESeq2")
# BiocManager::install("clusterProfiler") 如果需要用clusterProfiler做富集分析的话
```

**Mac下需要安装GNU-sed来替换OSX的sed**

```bash
## mac 的sed和linux 下的sed用法差别很大，我习惯了GNU-sed所以我在mac下装了gsed
## 如果没有安装homebrew按照：
/usr/bin/ruby -e "$(curl -fsSL https://cdn.jsdelivr.net/gh/ineo6/homebrew-install/install)"
## 安装好homebrew后
brew install gnu-sed
```

**后续富集分析调用了`TBtools`:**

- [CJ-Chen/TBtools](https://github.com/CJ-Chen/TBtools) ,
- [软件下载地址](https://github.com/CJ-Chen/TBtools/releases),
- 引用:[TBtools - an integrative toolkit developed for interactive analyses of big biological data](https://www.sciencedirect.com/science/article/abs/pii/S1674205220301878),
- DOI:  https://doi.org/10016/j.molp020.06.009.


#### 安装教程
- **安装：** 克隆到本地

```bash
## ./后面可以加你个性化的名字，最好不要有中文，也可以不加，克隆过去就是那个文件夹的名字就是OneStepWGCNA。
git clone https://gitee.com/shawnmagic/OneStepWGCNA.git ./
```
**升级：** 通过git pull更新
```bash
## cd到仓库路径下,直接git pull 例如
cd 15.gitee/OneStepWGCNA
git pull
```

#### 使用说明

**初始化脚本**，就是把脚本的绝对路径改成你电脑上文件夹的绝对路径：

- 这里首先把脚本路径改掉。
- 其次为了统一mac（mac catalina以后默认zsh）和linux，决定把alias写到根目录下`.bash_alias`下。

```bash
## 所有的R包，mac下gsed安装好后执行初始化脚本
sh init.sh 
你的操作系统是：1. MacOS 2. linux或者win10内建的bash. 填写1或者2:
## 按照提示填写数字1或者2，如果是mac确保你装了gsed，具体方法百度homebrew，然后用brew装gnu-sed
```


    
#### 数据准备

**Readcount矩阵**
样品在行，gene id在列如下表准备，excel的话最后保存成**制表符分隔符的txt格式**。


| geneid  | sample1 | sample2 | sample3 | ... |
|---------|---------|---------|---------|-----|
| geneid1 | 2       | 3       | 2       | ... |
| geneid2 | 2       | 3       | 8       | ... |
| geneid3 | 10      | 15      | 20      | ... |
| geneid4 | 221     | 231     | 227     | ... |
| ...     | ...     | ...     | ...     | ... |

**Traitdata**
这里对于数量性状和质量性状的输入进行了区分，脚本会按行数自动分辨
<font color = red>**必须保证traitdata的样品和上面readcount矩阵中样品一致，无论是数量还是名字**</font>
*对于数量性状（这里只可以测量的性状，比如株高，生育期等）*
样品在列，性状在行

| sample  | trait1 | trait2 | trait3 | ... |
|---------|--------|--------|--------|-----|
| sample1 | 5      | 1    | 300    | ... |
| sample2 | 6      | 0    | 190    | ... |
| sample3 | 2      | 2    | 400    | ... |
| ...     | ...    | ...    | ...    | ... |

*对于质量性状（比如种皮颜色，或者直接样本分组，比如case control，抗病，感病等）*

只有两列，第一列sample，第二列type：


| sample  | type    |
|---------|---------|
| sample1 | case    |
| sample2 | case    |
| sample3 | case    |
| sample4 | control |
| sample5 | control |
| sample6 | control |
| ...     | ...     |

#### WGCNA
如果你已经把`alias`搞定了，那么就直接用default参数跑一下

```R
## 建议在你的~/.bashrc(linux) 或者~/.zshrc(OSX 104之后) 或者~/.bash_profile（OSX104之前） 中加上 source ~/.bash_alias,这样每次当你打开terminal他会自动激活你的alias
source ~/.bash_alias
## 如果R在conda环境下，比如我单独给R建立了一个env
conda activate R
WGCNA -r readcountfile -t traitfile -w $PWD &
```
需要更改参数 -h 查看

```R
WGCNA -h
Usage: 11.09.OneStepWGCNA.R [-[-help|h]] [-[-readcount|r] <character>] [-[-traitData|t] <character>] [-[-RcCutoff|c] [<integer>]] [-[-samplePerc|p] [<double>]] [-[-RemainGeneNum|g] [<integer>]] [-[-output|o] [<character>]] [-[-workdir|w] <character>]
    -h|--help             help information
    -r|--readcount        inputfile: readcount matrix, geneID in row, sample names in column
    -t|--traitData        inputfile: trait data, if your data is quantitative traits, set sample names in row, trait in column, if it is discrete trait, set sample name in 1st column, levels in 2nd column
    -c|--RcCutoff         Noise remove: based on WGCNA FAQ, background noise should be removed, set the readcount value cutoff.(default = 6)
    -p|--samplePerc       Noise remove: At least how many samples have readcount value greater than cutoff,(Range 0-1, default = 0)
    -g|--RemainGeneNum    WGCNA gene input: After removing the background noise, the gene needs to be screened a second time, and the gene with the highest MAD value among the samples will be retained.(default = 15000)
    -o|--output           Output: project name for output file names.(default = system date)
    -w|--workdir          working directory, you have type -w $PWD or -w `pwd`
```

#### CRI-TM1 v2陆地棉一键模块富集分析+dotplot出图
这里用了TBtools的富集分析的工具，我只准备了`CRI-TM1 v2`参考基因组的GO和kegg文件，如果需要其他参考基因组可以自己构建`gene2go`和`gene2kid`然后替换掉02.TBtools下的，`CRI.TM1.V2TBtools.gene2ko CRI.TM1.V2.TBtools.gene2go`两个文件，如果不是植物的还需要下载自己需要的`backend`替换掉。人的话建议直接`clusterProfiler`做下游分析，已经很成熟了。 替换掉以后还有更改alias中对应的文件名字。

运行当然很简单：

在服务器上如果只是普通用户，很多系统的软件会有各种各样的问题，建议用conda搭建自己的分析环境。

```bash
WEnrich
是否在conda环境下用Rscript(0 or 1)： 1 ## 如果你的Rscript不在conda环境下写0 如果在写1
R所在的conda环境名字：R ##如果在conda下这里写你R存在的conda环境的名字
```

#### Demo
目前不会提供，可以翻一翻其他教程中的demo先测试下。

#### 致谢
感谢CJ大神的指导， 感谢活跃在简书，知乎各路大神对WGCNA的解读。

#### 参考文章：
1. [homebrew安装方法](https://zhuanlan.zhihu.com/p/90508170)
2. [WGCNA-FAQ](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/faq.html)
3. [生信技能树-WGCNA](https://mp.weixin.qq.com/s/HDCbJR4nfLlzeaDJmYO_gA)
4. [生信宝典-WGCNA](https://www.jianshu.com/p/e9cc3f43441d)
5. [简书-六六_ryx-WGCNA](https://www.jianshu.com/p/f0409a045dab)