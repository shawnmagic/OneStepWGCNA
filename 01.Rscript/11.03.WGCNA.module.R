WGCNA.oneStepNetWork <- function(Title){
  #######============一步法网络构建===========
  # power: 上一步计算的软阈值
  # maxBlockSize: 计算机能处理的最大模块的基因数量 (默认5000)；
  #  4G内存电脑可处理8000-10000个，16G内存电脑可以处理2万个，32G内存电脑可
  #  以处理3万个
  #  计算资源允许的情况下最好放在一个block里面。
  # corType: pearson or bicor
  # numericLabels: 返回数字而不是颜色作为模块的名字，后面可以再转换为颜色
  # saveTOMs：最耗费时间的计算，存储起来，供后续使用
  # mergeCutHeight: 合并模块的阈值，越大模块越少
  net = blockwiseModules(datExpr, power = power, maxBlockSize = nGenes,
                         TOMType = type, minModuleSize = 25,
                         reassignThreshold = 0, mergeCutHeight = 0,
                         numericLabels = TRUE, pamRespectsDendro = FALSE,
                         saveTOMs=TRUE, corType = corType, 
                         maxPOutliers=maxPOutliers, loadTOMs=TRUE,
                         #saveTOMFileBase = paste(Title,".tom",sep = ""),
                         verbose = 3)
  table(net$colors)
  assign("net",value = net, envir = globalenv())
  ####============modular construction=================
  ## 灰色的为**未分类**到模块的基因。
  # Convert labels to colors for plotting
  moduleLabels = net$colors
  assign("moduleLabels",value = moduleLabels, envir = globalenv())
  moduleColors = labels2colors(moduleLabels)
  assign("moduleColors",value = moduleColors, envir = globalenv())
  # Plot the dendrogram and the module colors underneath
  # 如果对结果不满意，还可以recutBlockwiseTrees，节省计算时间
  pdf(file = paste(Title,"Module.pdf",sep = "."),width = 6,height = 6)
  plotDendroAndColors(net$dendrograms[[1]], moduleColors[net$blockGenes[[1]]],
                      "Module colors",
                      dendroLabels = FALSE, hang = 0.03,
                      addGuide = TRUE, guideHang = 0.05)
  dev.off()
  # module eigengene, 可以绘制线图，作为每个模块的基因表达趋势的展示
  MEs = net$MEs
  ### 不需要重新计算，改下列名字就好
  ### 官方教程是重新计算的，起始可以不用这么麻烦
  MEs_col = MEs
  colnames(MEs_col) = paste0("ME", labels2colors(
    as.numeric(str_replace_all(colnames(MEs),"ME",""))))
  MEs_col = orderMEs(MEs_col)
  assign("MEs",value = MEs, envir = globalenv())
  assign("MEs_col",value = MEs_col, envir = globalenv())
  pdf(file = paste(Title,"Eigeng_adja_heatmap.pdf",sep = "."),width = 7,height = 10)
  # 根据基因间表达量进行聚类所得到的各模块间的相关性图
  # marDendro/marHeatmap 设置下、左、上、右的边距
  plotEigengeneNetworks(MEs_col, "Eigengene adjacency heatmap", 
                        marDendro = c(3,3,2,4),
                        marHeatmap = c(3,4,2,2), plotDendrograms = T, 
                        xLabelsAngle = 90)
  dev.off()
  Gene2module <- data.frame(GID = colnames(datExpr),
                            Module = moduleColors)
  write.table(Gene2module,file = paste(Title,"Gene2module.xls",sep = "_"),
              row.names = F,
              quote = F,
              sep = "\t")
}
